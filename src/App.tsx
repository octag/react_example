import { ChangeEvent, MouseEvent, useState } from 'react'
import './App.css'

interface Entry {
  idx: number;
  name: string;
  description: string;
}

interface TableEntryProps {
  entry: Entry;
  onDeleteEntry: () => void
}

interface NewEntryFormProps {
  onAddEntry: (e: Entry) => void
}

function TableEntry(props : TableEntryProps) {
  return(
    <tr>
      <td>{props.entry.idx}</td>
      <td>{props.entry.name}</td>
      <td>{props.entry.description}</td>
      <td><button onClick={(_: MouseEvent<HTMLButtonElement>) => props.onDeleteEntry()}>x</button></td>
    </tr>
  )
}

function NewEntryForm(props: NewEntryFormProps) {
  const [newEntry, setNewEntry] = useState<Entry>({idx: 0, name: '', description: ''})

  function handleAdd() {
    props.onAddEntry(newEntry)
    setNewEntry({
      idx: newEntry.idx + 1,
      name: '',
      description: ''
    })
  }

  return (
    <tr>
      <td>{newEntry.idx}</td>
      <td><input value={newEntry.name} onChange={(e) => setNewEntry({...newEntry, name: e.target.value})} type="text" placeholder='New name' /></td>
      <td><input value={newEntry.description} onChange={(e) => setNewEntry({...newEntry, description: e.target.value})} type="text" placeholder='New description' /></td>
      <td><button onClick={(_: MouseEvent<HTMLButtonElement>) => handleAdd()}>+</button></td>
    </tr>
  )
}

function Table() {
  const [entries, setEntries]  = useState<Entry[]>([])
  const [filter, setFilter] = useState<Entry>({idx: 0, name: '', description: ''})

  function handleDelete(entry: Entry): void {
    setEntries(entries.filter(e => e !== entry))
  }

  function handleAdd(newEntry: Entry): void {
    setEntries(entries.concat([newEntry]))
  }

  return (
    <table>
      <thead>
        <tr>
          <th>Idx</th>
          <th>Name</th>
          <th>Description</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td><input type="text" value={filter.name} onChange={(e: ChangeEvent<HTMLInputElement>) => setFilter({...filter, name: e.target.value})} placeholder='Filter by name' /></td>
          <td><input type="text" value={filter.description} onChange={(e: ChangeEvent<HTMLInputElement>) => setFilter({...filter, description: e.target.value})} placeholder='Filter by description' /></td>
          <td></td>
        </tr>
        {entries
          .filter(entry => entry.name.toLowerCase().includes(filter.name.toLocaleLowerCase()) &&
                  entry.description.toLowerCase().includes(filter.description.toLocaleLowerCase()) )
          .map(entry => <TableEntry key={entry.idx} entry={entry} onDeleteEntry={() => handleDelete(entry)}/>)}
        {/* New entry form is it's own component so that changes to the form don't trigger re-renders of the whole table*/}
        <NewEntryForm onAddEntry={handleAdd} />
      </tbody>
    </table>
  )
}

function App() {
  return(
    <>
      <Table />
    </>
  )
}

export default App

